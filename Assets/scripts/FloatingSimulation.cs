﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingSimulation : MonoBehaviour {

    public float m_uprightForce = 1000.0f;
    public float m_wobbleFrequency = 0.5f;
    public float m_waterlineWobbleAmplitude = 0.5f;
    public float m_uprightWobbleAmplitude = 0.5f;

    public float m_dragIntoWater = 3.0f;
    public float m_dragSidewards = 1.5f;
    public float m_dragForwards = 0.5f;

    public float m_angularDragAroundForward = 3.0f;
    public float m_angularDragAroundUp = 2.0f;
    public float m_angularDragAroundRight = 2.0f;

    private Rigidbody m_shipBody;
    private Transform m_waterline;
    private Transform m_keel;

    private float m_wobblePhase = 0;

    private const float TwoPi = Mathf.PI * 2.0f;
    
    void Awake()
    {
        m_shipBody = gameObject.GetComponent<Rigidbody>();

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            Transform child = gameObject.transform.GetChild(i);
            if (child.name == "waterline")
            {
                m_waterline = child;
            }
            else if (child.name == "keel")
            {
                m_keel = child;
            }
        }
    }

    float dragAcceleration(float currVel, float dragFactor)
    {
        //f(x) = currVel * e ^ -(ln(dt * dragFactor + 1))
        float velDir = Mathf.Sign(currVel);
        float dampedVelocity = Mathf.Abs(currVel) * Mathf.Exp(-Mathf.Log(1 + Time.fixedDeltaTime) * dragFactor);
        float dragAcceleration = -velDir * (Mathf.Abs(currVel) - dampedVelocity);
        return dragAcceleration;
    }

    void FixedUpdate()
    {
        m_wobblePhase += Time.fixedDeltaTime * TwoPi * m_wobbleFrequency;
        if (m_wobblePhase > TwoPi)
            m_wobblePhase -= TwoPi;
        float wobbleOffset = Mathf.Cos(m_wobblePhase) * m_waterlineWobbleAmplitude;
        float underwater = wobbleOffset + m_waterline.localPosition.y - gameObject.transform.position.y;

        // damping
        Vector3 vel = m_shipBody.velocity;
        Vector3 relVel = Quaternion.Inverse(transform.rotation) * m_shipBody.velocity;
        Vector3 relAngVel = Quaternion.Inverse(transform.rotation) * m_shipBody.angularVelocity;
        
        if (vel.y < 0.0f && m_keel.position.y < 0.0f) // damping when ship is pushed into water
        {
            float verticalDamping = dragAcceleration(vel.y, m_dragIntoWater);
            m_shipBody.AddForce(0.0f, verticalDamping, 0.0f, ForceMode.VelocityChange);
        }
        m_shipBody.AddRelativeForce(dragAcceleration(relVel.x, m_dragSidewards), 0.0f, dragAcceleration(relVel.z, m_dragForwards), ForceMode.VelocityChange);

        // angular damping
        m_shipBody.AddRelativeTorque(dragAcceleration(relAngVel.x, m_angularDragAroundRight),
                                     dragAcceleration(relAngVel.y, m_angularDragAroundUp),
                                     dragAcceleration(relAngVel.z, m_angularDragAroundForward), 
                                     ForceMode.VelocityChange);

        Quaternion uprightRotation = Quaternion.Euler(Mathf.Cos(m_wobblePhase) * m_uprightWobbleAmplitude, Mathf.Sin(m_wobblePhase) * m_uprightWobbleAmplitude, 0.0f);
        Vector3 targetUp = uprightRotation * (new Vector3(0.0f, 1.0f, 0.0f));

        // keep upright
        Vector3 torqueAxis = Vector3.Cross(gameObject.transform.up, targetUp);
        float dirFactor = 0.2f + 1.0f - Mathf.Abs(Vector3.Dot(gameObject.transform.up, targetUp));
        Vector3 torque = torqueAxis * Mathf.Clamp01(dirFactor * m_uprightForce);
        m_shipBody.AddTorque(torque, ForceMode.Acceleration);

        // buoyancy
        float buoyancyAccelerationPerMeter = -Physics.gravity.y * 0.5f;
        float buoyancyAcceleration = underwater * buoyancyAccelerationPerMeter;
        if (buoyancyAcceleration > 0)
        {
            m_shipBody.AddForce(0.0f, buoyancyAcceleration, 0.0f, ForceMode.Acceleration);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
