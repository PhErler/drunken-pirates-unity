﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootCannon : MonoBehaviour {

    // https://en.wikipedia.org/wiki/Naval_artillery_in_the_Age_of_Sail
    // https://en.wikipedia.org/wiki/Rating_system_of_the_Royal_Navy
    // 

    public bool m_controlledByLocalPlayer = true;

    public float m_reloadTime = 3.0f;
    public float m_muzzleVelocity = 100.0f;
    public GameObject m_prefabCannonball;
    public GameObject[] m_cannonPositions;

    private float m_timeSiceLastShot = 0.0f;

    void Awake()
    {

    }

	void Start ()
    {
		
	}
    
    // Update is called once per frame
    void Update ()
    {
        m_timeSiceLastShot += Time.deltaTime;

        if (m_controlledByLocalPlayer && Input.GetAxis("Fire2") == 1.0f && m_timeSiceLastShot > m_reloadTime)
        {
            m_timeSiceLastShot = 0;

            foreach (GameObject cannon in m_cannonPositions)
            {
                Vector3 spawnPos = cannon.transform.position;
                GameObject cannonball = Instantiate(m_prefabCannonball, spawnPos, Random.rotation) as GameObject;
                Rigidbody cannonballRb = cannonball.GetComponent<Rigidbody>();
                //cannonball.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);
                //cannonballRb.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);
                Vector3 shootDir = cannon.tag == "LeftCannon" ? -transform.right : transform.right;
                cannonballRb.AddForce(shootDir * m_muzzleVelocity, ForceMode.VelocityChange);
                Physics.IgnoreCollision(cannonball.GetComponent<Collider>(), GetComponent<Collider>());
            }
        }
    }
}
