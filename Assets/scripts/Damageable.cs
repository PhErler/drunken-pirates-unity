﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour {

    public float m_initialHealthPoints = 100.0f;

    private float m_currentHealthPoints = 100.0f;

    public float GetHealthPoints()
    {
        return m_currentHealthPoints;
    }

    public void Impact(Collision coll, Rigidbody projectile, float projectileDmgFactor)
    {
        const float OneOverSqrtTwo = 0.7071067812f;

        Vector3 avgContactPoint = Vector3.zero;
        foreach (ContactPoint p in coll.contacts)
        {
            avgContactPoint += p.point;
        }
        avgContactPoint /= (float)coll.contacts.Length;

        Vector3 relativeContactPoint = avgContactPoint - coll.transform.position;

        float shipForwardDotContactDir = Vector3.Dot(coll.transform.forward, relativeContactPoint.normalized);
        float armorFactor = 1.0f;
        if (shipForwardDotContactDir > OneOverSqrtTwo)
        {
            armorFactor = 0.5f;
        }
        else if (shipForwardDotContactDir < -OneOverSqrtTwo)
        {
            armorFactor = 2.0f;
        }

        float impulse = coll.relativeVelocity.magnitude * projectile.mass;

        float damage = impulse * armorFactor * projectileDmgFactor;
        Debug.Log("Impactdamage: " + damage);
        Damage(damage);
    }

    public void Damage(float amount)
    {
        m_currentHealthPoints -= amount;
        HealthChanged();
    }
    public void Heal(float amount)
    {
        m_currentHealthPoints += amount;
        HealthChanged();
    }

    private void HealthChanged()
    {
        if (m_currentHealthPoints < 0.0f)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
