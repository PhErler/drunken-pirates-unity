﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipSteering : MonoBehaviour {

    public bool m_controlledByLocalPlayer = true;

    public float m_secToFullThrottle = 3.0f;
    public float m_secToFullTurn = 1.5f;

    public float m_maxVelocityMS = 45.0f;
    public float m_minTorqueFactorAtMaxVel = 0.1f;

    public float m_throttleForce = 10000000.0f;
    public float m_turnTorque    = 10000000.0f;

    protected float m_throttle = 0.0f;
    protected float m_turn = 0.0f;

    private Rigidbody m_shipBody;

    public Slider m_turnSlider;
    public Slider m_throttleSlider;
    

    public void GuiChangedTurn()
    {
        if (m_controlledByLocalPlayer)
        {
            m_turn = m_turnSlider.value;
        }
    }
    public void GuiChangedThrottle()
    {
        if (m_controlledByLocalPlayer)
        {
            m_throttle = m_throttleSlider.value;
        }
    }

    void Awake()
    {
        m_shipBody = gameObject.GetComponent<Rigidbody>();
        if (m_controlledByLocalPlayer)
        {
            m_turnSlider.onValueChanged.AddListener(delegate { GuiChangedTurn(); });
            m_throttleSlider.onValueChanged.AddListener(delegate { GuiChangedThrottle(); });
        }
    }
	
    void FixedUpdate()
    {
        // forward
        m_shipBody.AddRelativeForce(0.0f, 0.0f, m_throttleForce * m_throttle, ForceMode.Force);

        // turn
        float currForwardVelocity = (Quaternion.Inverse(transform.rotation) * m_shipBody.velocity).z;
        float velFactor = Mathf.Clamp01(currForwardVelocity / m_maxVelocityMS);

        float turnTorqueFactor = 0.0f;
        if (velFactor < 0.25f)
        {
            turnTorqueFactor = 8.0f * velFactor * velFactor;
        }
        else if (velFactor >= 0.25f && velFactor < 0.75f)
        {
            float x = velFactor - 0.5f;
            turnTorqueFactor = 1.0f - 8.0f * x * x;
        }
        else
        {
            float x = velFactor - 1.0f;
            turnTorqueFactor = Mathf.Clamp(8.0f * x * x, m_minTorqueFactorAtMaxVel, 1.0f);
        }

        float turnThrottle = m_turnTorque * m_turn * turnTorqueFactor;
        m_shipBody.AddTorque(Vector3.Scale(transform.up, new Vector3(turnThrottle, turnThrottle, turnThrottle)), ForceMode.Force);
    }

    // Update is called once per frame
    void Update ()
    {
        if (m_controlledByLocalPlayer)
        {
            m_throttle = Mathf.Clamp01(m_throttle + Input.GetAxis("Vertical") * Time.deltaTime * (1.0f / m_secToFullThrottle));
            m_turn = Mathf.Clamp(m_turn + Input.GetAxis("Horizontal") * Time.deltaTime * (1.0f / m_secToFullTurn), -1.0f, 1.0f);

            m_turnSlider.value = m_turn;
            m_throttleSlider.value = m_throttle;
        }
    }

    void OnDisable()
    {
        if (m_controlledByLocalPlayer)
        {
            m_turnSlider.onValueChanged.RemoveListener(delegate { GuiChangedTurn(); });
            m_throttleSlider.onValueChanged.RemoveListener(delegate { GuiChangedThrottle(); });
        }
    }
}
