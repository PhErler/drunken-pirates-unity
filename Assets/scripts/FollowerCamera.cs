﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerCamera : MonoBehaviour {

    public GameObject m_followThis;
    public Vector3 m_targetLocalPosition = new Vector3(0.0f, 28.0f, -55.0f);
    public Vector3 m_targetLocalRotation = new Vector3(12.88f, 0.0f, 0.0f);

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (m_followThis)
        {
            transform.localPosition = m_followThis.transform.position + m_followThis.transform.rotation * m_targetLocalPosition;
            transform.localRotation = m_followThis.transform.rotation * Quaternion.Euler(m_targetLocalRotation);
        }
	}
}
