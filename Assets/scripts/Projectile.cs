﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private Rigidbody m_projectileRB;
    public float m_dmgFactor = 1.0f;

    void Awake ()
    {
        m_projectileRB = gameObject.GetComponent<Rigidbody>();
        m_projectileRB.WakeUp();
    }
    
	void Start ()
    {
        Destroy(gameObject, 10.0f);
    }
	
    void OnCollisionEnter(Collision collision)
    {
        Damageable otherDamageable = collision.gameObject.GetComponent<Damageable>();
        if (otherDamageable)
        {
            otherDamageable.Impact(collision, m_projectileRB, m_dmgFactor);
        }

        Destroy(gameObject);
    }

	// Update is called once per frame
	void Update ()
    {
        if (transform.position.y < -10.0f || m_projectileRB.IsSleeping())
        {
            Destroy(gameObject);
        }

    }
}
